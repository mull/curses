require 'curses'
require 'pry'

Curses.init_screen
Curses.stdscr.clear

window = Curses::Window.new(5, 10, 0, 0)

pad = Curses::Pad.new(2, 8)
pad.setpos(0, 0)
pad.addstr("12345678")
pad.setpos(1, 0)
pad.addstr("01234567")

window.copy(pad, 0, 0, 0, 0, 2, 8, false)

window.getch
Curses.close_screen

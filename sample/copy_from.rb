require 'curses'
require 'pry'

Curses.init_screen
Curses.stdscr.clear

window = Curses::Window.new(5, 10, 0, 0)

pad = Curses::Pad.new(2, 8)
pad.setpos(0, 0)
pad.addstr("12345678")
pad.setpos(1, 0)
pad.addstr("01234567")

# Copy 2 rows: 0, 1
# Copy 8 cols: 0, 1, 2, 3, 4, 5, 6, 7
window.copy_from(pad, 0, 0, 0, 0, 1, 7, false)

# expect: source_min_row cannot be less than 0
# window.copy_from(pad, -1, 0, 0, 0, 1, 1, false)

# expect: source_min_col cannot be less than 0
# window.copy_from(pad, 0, -10, 0, 0, 1, 1, false)

# expect: destination_min_row cannot be less than 0
# window.copy_from(pad, 0, 0, -1, 0, 1, 1, false)

# expect: destination_min_col cannot be less than 0
# window.copy_from(pad, 0, 0, 0, -1, 1, 1, false)

# expect: destination_max_row must be <= destination_min_row 
# window.copy_from(pad, 0, 0, 2, 0, 1, 1, false)

# expect: destination_max_col must be <= destination_min_col
# window.copy_from(pad, 0, 0, 0, 2, 1, 1, false)

# expect: source_min_row is out of position (RuntimeError)
# window.copy_from(pad, 2, 0, 0, 0, 1, 7, false)

# expect: source_min_col is out of position (RuntimeError)
# window.copy_from(pad, 0, 8, 0, 0, 0, 0, false)

# expect: source is not large enough for copy
# why: because 2 means row [0, 1, 2], i.e. 3... yay!
# window.copy_from(pad, 0, 0, 0, 0, 2, 0, false)

# expect: source is not large enough for copy
# why: because 8 means column [0, 1, 2, 3, 4, 5, 6, 7], i.e. 7... yay!
# window.copy_from(pad, 0, 0, 0, 0, 0, 7, false)


# window.resize(1, 7)
# expect: self is not large nough for copy
# why: 1 row = [0, 1], but we only have [0] available
# window.copy_from(pad, 0, 0, 0, 0, 1, 6, false)

# expect: self is not large nough for copy
# why: 7 col = [0, 1, 2, 3, 4, 5, 6, 7], but we only have [0, 1, 2, 3, 4, 5, 6] available
# window.copy_from(pad, 0, 0, 0, 0, 0, 6, false)

window.getch
Curses.close_screen

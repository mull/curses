require 'curses'

include Curses

init_screen

# Our our main window is as big as the screen
main_window = Curses::Window.new(0, 0, 0, 0)
main_window.clear

# This text will get overwritten.
# Try setting the modifier (last argument to copywin) to true see the overlay effect.
main_window << "Hello from Window"
main_window.refresh

# Our pad is (probably) bigger
pad = Curses::Pad.new(800, 800)
pad.setpos(500, 500)
pad << "Hello from Pad"

# Start at position 500, 500 on pad
# Start at position 0, 0 on main_window
# Copy 3 lines and 25 columns
main_window.copy_from(pad, 500, 500, 0, 0, 3, 25, false)
main_window.refresh

main_window.getch

close_screen
